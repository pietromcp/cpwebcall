import React from 'react'
import WebRtcVideoCall from "./WebRtcVideoCall"
import { CPWebCallConfiguration } from "../config";
import { WithWebSocket } from './WithWebSocket';

interface WebRtcVideoCallContainerProps {
  config: CPWebCallConfiguration
}

function WebRtcVideoCallContainer({ config }: WebRtcVideoCallContainerProps) {
  return <WithWebSocket url={`${config.wsEndpoint}/signal`} render={(socket) => <WebRtcVideoCall config={config} socket={socket} />} />
}

export default WebRtcVideoCallContainer
