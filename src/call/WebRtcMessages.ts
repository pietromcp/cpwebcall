export interface OfferMessage {
    username: string
    peerUsername: string
    type: 'video-offer'
    sdp: RTCSessionDescription
}

export interface VideoAnswerMessage {
    username: string
    peerUsername: string
    type: 'video-answer'
    sdp: RTCSessionDescription
}

export interface IceCandidateMessage {
    username: string
    peerUsername: string
    type: 'new-ice-candidate'
    candidate: RTCIceCandidate
}

export interface NameChoiceMessage {
    type: 'name-choice'
    username: string
}

export type Message = OfferMessage | IceCandidateMessage | VideoAnswerMessage | NameChoiceMessage
