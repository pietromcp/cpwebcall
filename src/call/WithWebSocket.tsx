import React, {useEffect, useState} from 'react'

export interface WithWebSocketProps {
  url: string
  render: (socket: WebSocket) => any,
  log?: (msg?: any, ...args: any[]) => void
}

export function WithWebSocket({url, render, log}: WithWebSocketProps) {
  const trace = log || console.log
  const [socket, setSocket] = useState<WebSocket | undefined>(undefined)
  const [opened, setOpened] = useState<boolean>(false)
  useEffect(() => {
    trace('Opening WS (effect)')
    const theSocket = new WebSocket(url)
    theSocket.addEventListener('open', function (event: Event) {
      trace('WSS opened', socket)
      setOpened(true)
    })
    setSocket(theSocket)
  },// eslint-disable-next-line react-hooks/exhaustive-deps
   [])
  if(socket && opened) {
    return (render(socket))
  }
  return <></>
}
