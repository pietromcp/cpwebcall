import React, {useEffect, useState} from 'react'
import {IceCandidateMessage, Message, NameChoiceMessage, OfferMessage, VideoAnswerMessage} from "./WebRtcMessages";
import {CPWebCallConfiguration} from "../config";
import axios from 'axios'

// const shareAudio = true

const animals = ['cat', 'dog', 'donkey', 'monkey', 'hedgehog', 'pig', 'tiger', 'lion', 'tortoise', 'bat', 'mouse']
const adjectives = ['sad', 'happy', 'hungry', 'angry', 'funny', 'old', 'young', 'honest', 'red', 'pink', 'green']

const random = (limit: number) => {
    return Math.floor(limit * Math.random())
}

const username = `${adjectives[random(adjectives.length)]} ${animals[random(animals.length)]}`

// const offerOptions: RTCOfferOptions = {
//     offerToReceiveAudio: shareAudio,
//     offerToReceiveVideo: true
// }

// const connectionConfig: RTCConfiguration = {
//     iceServers: [{
//         urls: ['stun:stun.l.google.com:19302']
//     }]
// }

//
const connectionConfig: RTCConfiguration = {
    iceServers: [{
             urls: ['stun:stun.l.google.com:19302']
         }, {
              urls: ['stun:turn.frozenmountain.com:3478']
          },{
           urls: ['turn:relay.backups.cz'],
           username: 'webrtc',
           credential: 'webrtc'
         }, {
           urls: ['turn:turn.frozenmountain.com:80'],
           username: 'test',
           credential: 'pa5w0rd!'
         }, {
           urls: ['turns:turn.frozenmountain.com:443'],
           username: 'test',
           credential: 'pa5w0rd!'
         }/*, {
           urls: ['turn:numb.viagenie.ca'],
           username: 'webrtc@live.com',
           credential: 'muazkh'
         }*/]
}

function handlerFor(descr: string): (this: RTCPeerConnection, e: Event) => any {
    return function (e: Event) {
        console.log(`${descr}: ${this.connectionState} ${JSON.stringify(e)}`)
    }
}

interface WebRtcVideoCallProps {
    config: CPWebCallConfiguration
    socket: WebSocket
}

function WebRtcVideoCall({ socket, config }: WebRtcVideoCallProps) {
    const [localVideoRef] = useState(React.createRef<HTMLVideoElement>())
    const [remoteVideoRef] = useState(React.createRef<HTMLVideoElement>())
    const [users, setUsers] = useState<string[]>([])
    const [toggleRefreshUsers, setToggleRefreshUsers] = useState<boolean>(false)
    const [peerUsername, setPeerUsername] = useState('')
    const [shareAudio, setShareAudio] = useState<boolean>(true)
    const [shareVideo, setShareVideo] = useState<boolean>(false)

    const setPeerConnection = (pc: RTCPeerConnection) => {
        const win = window as any
        win.peerConnection = pc
        console.log('window.peerConnection', win.peerConnection)
    }

    const peerConnection = (): RTCPeerConnection => {
        const win = window as any
        console.log('Getting peerConnection from window', win.peerConnection)
        return (window as any).peerConnection as RTCPeerConnection
    }

    const sendToServer = (msg: Message) => {
        console.log(`SEND MESSAGE ${msg.type}`)
        socket.send(JSON.stringify(msg))
    }

    useEffect(() => {
        socket.addEventListener('message', async function (msgEvent) {
            const data = JSON.parse(msgEvent.data) as Message
            console.log(`Received message '${data.type}' from '${data.username}'`)
            await handleIncomingMsg(data)
        })

        sendToServer({
            type: 'name-choice',
            username: username
        } as NameChoiceMessage)
    },// eslint-disable-next-line react-hooks/exhaustive-deps
     [socket])

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        axios.get(`${config.apiEndpoint}/users`)
            .then(response => setUsers((response.data as string[]).filter(u => u !== username)))
    }, [config.apiEndpoint, toggleRefreshUsers])

    async function handleNegotiationNeededEvent(this: RTCPeerConnection, e: Event) {
        const offer = await this.createOffer()
        await this.setLocalDescription(offer)
        const msg = {
            username: username,
            peerUsername: peerUsername,
            type: "video-offer",
            sdp: this.localDescription
        } as OfferMessage
        sendToServer(msg)
    }

    async function handleIncomingMsg(rawMessage: Message) {
      console.log(`Message '${rawMessage.type}' received:`, rawMessage)
        if(rawMessage.type === "video-offer") {
            const msg: OfferMessage = rawMessage as OfferMessage
            const theStream = localStream() as MediaStream
            if(msg && theStream) {
                const thePc = createPeerConnection(msg.username)
                setPeerConnection(thePc)
                setPeerUsername(msg.username)
                const desc = new RTCSessionDescription(msg.sdp);
                await thePc.setRemoteDescription(desc)
                // const stream = await navigator.mediaDevices.getUserMedia({ audio: shareAudio, video: shareVideo })
                // setLocalStream(stream)
                theStream.getTracks().forEach(track => thePc!.addTrack(track, theStream))
                const answer = await thePc.createAnswer()
                await thePc.setLocalDescription(answer)
                const outMsg = {
                    username: username,
                    peerUsername: msg.username,
                    type: "video-answer",
                    sdp: thePc.localDescription
                } as VideoAnswerMessage
                sendToServer(outMsg)
            }
        } else if(rawMessage.type === "new-ice-candidate") {
            const msg: IceCandidateMessage = rawMessage as IceCandidateMessage
            console.log('handle new-ice-candidate', msg, peerConnection())
            try {
                if(peerConnection()) {
                    await peerConnection().addIceCandidate(msg.candidate)
                } else {
                    console.error('Received handle new-ice-candidate but peerConnection is null')
                }
                console.log('1111 --> ', msg.candidate, 'pc = ', peerConnection())
            } catch(err) {
                console.log('0000 --> ', msg.candidate, '|', err, '|', JSON.stringify(err), '|', 'pc = ', peerConnection())
            }
        } else if (rawMessage.type === "video-answer") {
            const msg: VideoAnswerMessage = rawMessage as VideoAnswerMessage
            var desc = new RTCSessionDescription(msg.sdp)
            try {
                await peerConnection().setRemoteDescription(desc)
            } catch(err) {
                console.error('Error handling video-answer', err)
            }

        }
    }

    function handleTrackEvent(event: RTCTrackEvent) {
        console.log('!!! TRACK EVENT !!!', event.streams.length, event.streams[0])
        event.streams[0].onaddtrack = (ev: MediaStreamTrackEvent) => {
          console.log('track added')
        }
        setRemoteStream(event.streams[0])
    }

    function handleICECandidateEvent(peer: string) {
        return (event: RTCPeerConnectionIceEvent) => {
            if (event.candidate) {
                sendToServer({
                    type: "new-ice-candidate",
                    username: username,
                    peerUsername: peer,
                    candidate: event.candidate
                } as IceCandidateMessage);
            }
        }
    }

    function createPeerConnection(peerUsername: string): RTCPeerConnection {
        const thePc = new RTCPeerConnection(connectionConfig)
        thePc.onicecandidate = handleICECandidateEvent(peerUsername)
        thePc.ontrack = handleTrackEvent
        thePc.onnegotiationneeded = handleNegotiationNeededEvent
        // thePc.onremovetrack = handlerFor('onremovetrack')
        thePc.oniceconnectionstatechange = handlerFor('oniceconnectionstatechange')
        thePc.onicegatheringstatechange = handlerFor('onicegatheringstatechange')
        thePc.onsignalingstatechange = handlerFor('onsignalingstatechange')
        return thePc
    }


    const localStream = (): MediaStream | null => {
        return localVideoRef && localVideoRef.current && localVideoRef.current.srcObject as MediaStream
    }

    const setLocalStream = (stream: MediaStream | null) => {
        if (localVideoRef.current) {
            localVideoRef.current.srcObject = stream;
        }
    }

    const setRemoteStream = (stream: MediaStream | null) => {
        if (remoteVideoRef.current) {
            remoteVideoRef.current.srcObject = stream;
        }
    }

    // const clearStream = () => setLocalStream(null)

    const handleSuccess = (stream: MediaStream) => {
        setLocalStream(stream)
    }
    const handleError = (err: any) => {
        console.error(err)
    }
    const openCamera = async (e: React.MouseEvent<HTMLElement>) => {
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ audio: shareAudio, video: shareVideo });
            handleSuccess(stream)
            e.stopPropagation()
            e.preventDefault()
        } catch (e) {
            handleError(e);
        }
    }

    // const closeCamera = () => {
    //     const theStream = localStream()
    //     if (theStream) {
    //         theStream.getTracks().forEach(t => t.stop())
    //         clearStream()
    //     }
    // }

    function startCall() {
        console.log('Starting call')
        const thePc = createPeerConnection(peerUsername)
        console.log('PC created')
        const stream = localStream()
        if(stream) {
            stream.getTracks().forEach(track => thePc.addTrack(track, stream))
        }
        setPeerConnection(thePc)
    }

    const refreshUsers = () => {
        setToggleRefreshUsers(!toggleRefreshUsers)
    }

    return <div style={{border: '1px solid white', width: '90%'}}>
            <video style={{ width: '45%', margin: '5px'}} ref={localVideoRef} autoPlay playsInline muted></video>
            <video style={{ width: '45%'}} ref={remoteVideoRef} autoPlay playsInline></video>
        <hr />
        <div style={{width: '100%'}}>
            <p><span>Username: <strong>{username}</strong> </span></p>
            <p><span>Peer's username: <strong>{peerUsername}</strong> </span></p>
            <p>Available users: {users.map(u => <span key={u} onClick={() => setPeerUsername(u)}>{u}</span>)} <button onClick={refreshUsers}>Refresh</button></p>
            <input type="checkbox" checked={shareAudio} onChange={() => setShareAudio(!shareAudio)} />Share audio
            <input type="checkbox" checked={shareVideo} onChange={() => setShareVideo(!shareVideo)} />Share video
            <button onClick={openCamera}>Open camera</button>
            <button onClick={startCall}>Start call</button>
        </div>
    </div>
}

export default WebRtcVideoCall
