import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Config } from './config'
import WebRtcVideoCallContainer from './call/WebRtcVideoCallContainer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <WebRtcVideoCallContainer config={Config} />
    </div>
  );
}

export default App;
