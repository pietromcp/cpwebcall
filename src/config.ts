const endpointOf = (protocol: string, hostAndPort: string) => `${protocol}://${hostAndPort}`

const host = 'my-signalling.herokuapp.com'
const apiProtocol = 'https'
const wsProtocol = 'wss'

export const Config = {
    apiEndpoint: endpointOf(apiProtocol, host),
    wsEndpoint: endpointOf(wsProtocol, host)
} as CPWebCallConfiguration

export interface CPWebCallConfiguration {
    apiEndpoint: string
    wsEndpoint: string
}
